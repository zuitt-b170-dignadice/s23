// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/
db.users.insert({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@gmail.com"
    },
    department: "none"
});

// Insert Many
/*
    - Syntax
        - db.collectionName.insertMany([ {objectA}, {objectB} ]]);
*/
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neil.a@gmail.com"
        },
        department: "science"
    },
    {
        firstName: "Neil",
        lastName: "Diamond",
        age: 46,
        contact: {
            phone: "87654321",
            email: "neil.d@gmail.com"
        },
        department: "music"
    }
]);

// [Section] Finding documents (Read)
// Find
/*
    - If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
    - This is based from the order that documents are stored in a collection
    - If a document is not found, the terminal will respond with a blank line
    - Syntax
        - db.collectionName.find();
        - db.collectionName.find({ field: value });
*/

// Finding a single document
// Leaving the search criteria empty will retrieve ALL the documents
db.users.find();
db.users.find({query}, {projection})

db.users.find({ firstName: "Stephen" });
db.users.find(
    {
        firstName : "Stephen",
        lastName: "Hawking",
        age: "76"
    } // as long one of the projection 
)
/* 
 query - specifies selection filter useing query operators. 

 projection - optional

 Comparison Operators
    equality
    strict equality
    non equality
    strict non equality

    $eq - matches values that are equal to a specified value
    %gte - greater thann
    $gt - matches values that are greater than a specified value 
    $lte - matches values that are less tahn or equal to a specified value'

    $and 
    $not - inverts the effect of a query expression and returns documents that do not match the query expression
    $nor
    $or - joins query clauses with a logical OR returns all documents


    S Y N T A X 
        { field : { $gt: value}}


    
*/

// https://www.mongodb.com/docs/manual/crud/

// The "pretty" method allows us to be able to view the documents returned by our terminals in a better format
    // Note that this is mostly applicable in terminals and older versions of robo3t (1.1 and older)
db.users.find({ firstName: "Stephen" }).pretty();

// Finding documents with multiple parameters
/*
    - Syntax
        - db.collectionName.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ lastName: "Armstrong", age: 82 })




/* 
    Finding documents with the use of query comparison operator.
*/

db.users.find(
    {
        age : { $gt : 50}
    }
);

db.users.find(
    {
        age : { $eq : 75}
    }
);

// same with 

db.users.find(
    {
        age : 75
    }
);

// $in 
// { field : { $in : [<value1>, <value2, .. <valueN>]}}

db.users.find(
    {
        firstName : { $in : ["jack", "john"] }
    }
)

db.users.find(
    {
        lastName : { $in : ["Doe", "Armstrong", "Hawking"]}
    }
)

// Logical Operator 
/* 
       Finding documents with the use of query logical operator.
*/


// $and - 
// { $and: [ { <expression1> }, { <expression2> } , ... , { <expressionN> } ] }

db.users.find(
    {
        $and : [
                { firstName : "jane"},
                { lastName : "Doe"}
        ]
    }
)
// same with
db.users.find(
    {
                firstName : "jane",
                lastName : "Doe"
    }
)

// $or - 
// { $or: [ { <expression1> }, { <expression2> } , ... , { <expressionN> } ] }

db.users.find(
    {
        $or : [
                { firstName : "jane"},
                { lastName : "Doe"}
        ]
    }
)


// using or logical operator

db.users.find(
    {
        $or: [
            { firstName : "Stephen"},
            { lastName : "Doe"},
            { age : {$gte : 80}}
        ]
    }

)

// $regex - selects documents where values match regular expression

/* 
    Provides regular expression capabilities for pattern matching strings in queries
*/

// using regex operator 

db.users.find(
    {
        firstName : { $regex : "s", $optionss : "i",}
    }
)

/* 
{ <field>: { $regex: /pattern/, $options: '<options>' } }
{ <field>: { $regex: 'pattern', $options: '<options>' } }
{ <field>: { $regex: /pattern/<options> } }
*/

// projection 
db.users.find({query}, {projection})

// FInding documents with the use of query and field projection

db.users.find(
    {
        _id : ObjectId("624edf5970ab7a385d4667bb")
    },
    {
        fName : 1,
        age : 1,
        _id : 0
    }
)

// id will always be included unless we explicitly exxlude it


// Look for a document that has a first name of neil and display only the first name and phone number

db.users.find(
    {
        _id : ObjectId("624edf5970ab7a385d4667bb")
    },
    {
        fName : 1,
        _id : 0,
        contactInfo : { mobile : 1 }
    }
)

db.users.find(
    {
        fName : "Neil"
    },
    {
        fName : 1,
        mobile : 1,
        _id : 0
    }

)

// Update in CRUD 
    // update operations modify existing documents in a collection. MOngoDB provides the following methosds to update documents of a collection
// db.collections.updateOne({filter}, {update})
// db.collections.updateMany()
// db.collections.replaceOne()

    //  we will insert a dummy document to be able to use for update operation

    // test 


    db.users.insertOne(
        {
            fName : "Test",
            lname : "Test",
            age : 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    )
    

 
    db.users.updateOne(
        {
            fName : "Test"
        },
        {
            $set : {
                fName : "Joy",
                lname : "Pague",
                age : 16,
                contact: {
                    phone: "1232354123456",
                    email: "joy@mail.com"
                },
                department: "none"
            }
        }
        
    )

    //  update the newly updated document with department to instructor departments
    
    db.users.updateOne(
        {
            fName : "Joy"
        },
        {
            $set : {
                department: "Instructor Department"
            }
        }
        
    )

    // remove the specified field using &unset update operator 

    db.users.updateOne(
        {
            fName : "Joy"
        },
        {
            $unset : {
                department: "Instructor Department"
            }
        }
        
    )

    // WHat if i got multiple documents from the filter parameter and I only want to update one document
        // insert a dummy document to be able to use for update operation

    db.users.insertMany(
        [
            {
                fName : "Test",
                lname : "Test",
                age : 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            },
            {
                fName : "Test",
                lname : "Test",
                age : 0,
                contact: {
                    phone: "0",
                    email: "test@mail.com"
                },
                department: "none"
            }
        
        ]
    )
//  Using update one
    db.users.updateOne( //will update the first object
        {
            fName : "Test"
        },
        {
            $set: {
                fName : "Jherson",
                lname : "Dignadice",
                age : 23,
                contact: {
                    phone: "2134532",
                    email: "jherson@mail.com"
                },
                department: "none"
            }
        }
    )

// Find all documents with department field and update to HR department

db.users.updateMany( 
    {  
        department: {$exists: true}
 
    },
    { 
       $set:{ 
            department : "HR"
        }
    }
 );
    
 // Replaces a singe document within ithe collection based on the filter

 // fully replaces the document without the parameter

 /* 
 db.collection.replaceOne(
   <filter>,
   <replacement>,
   {
     upsert: <boolean>,
     writeConcern: <document>,
     collation: <document>,
     hint: <document|string>                   // Available starting in 4.2.1
   }
)
 */

 db.users.replaceOne(
     {
         fName : "Test"
     },
     {
        fName : "Johnny",
        lName : "Cuyno"
     }
 )

 // Delete operations  - remove documents from a collection. MongoDB provides the following methods to delet documents of a collection

//  db.collections.deleteOne({filter})
// db.collections.deleteManty({filter})
 // Insert dummy test
 db.users.insertOne(
    
        {
            fName : "Test",
            lname : "Test",
            age : 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    
)

// to delete an existing single document, use deleteOne method
db.users.deleteOne(
    {
        fName : "Test"
    }
)

// Insert dummy documents for deleteMany() method

db.users.insertMany(
    [
        {
            fName : "Test",
            lname : "Test",
            age : 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        },
        {
            fName : "Test",
            lname : "Test",
            age : 0,
            contact: {
                phone: "0",
                email: "test@mail.com"
            },
            department: "none"
        }
    
    ]
)

// delete multiple documents
db.users.deletemany(
    {
        lName : "Test"
    }
)

// deleting multiple documents targetting same field with different values

db.users.deleteMany(
    {
        fName : { $in: ["Joy", "Hannah", "Johnny"]}
    }
)


/* 
    create a hotel database and create several documents for rooms, manupulate the data using mongo
*/



/* 
db.collection.updateOne(
   <filter>,
   <update>,
   {
     upsert: <boolean>,
     writeConcern: <document>,
     collation: <document>,
     arrayFilters: [ <filterdocument1>, ... ],
     hint:  <document|string>        // Available starting in MongoDB 4.2.1
   }
)
*/

// update - modifications to apply 
// upsert - boolean (optional)
// write concern 


// create hotel database









