
// insertOne (single)

db.rooms.insertOne(
    {
        name_r : "single",
        accomdates : "2",
        price : 1000,
        description : "a simple room with all the basic necessities",
        rooms_avail : 10,
        isAvailable : false
    }
)

// insertMany (double)
db.rooms.insertMany(
    [

        {
            name_r : "double",
            accomdates : "3",
            price : 2000,
            description : "a room fit for a small family going on a vacation",
            rooms_avail : 5,
            isAvailable : false
        },
        {
            name_r : "double",
            accomdates : "3",
            price : 2000,
            description : "a room fit for a small family going on a vacation",
            rooms_avail : 5,
            isAvailable : false
        }
    
    ]
) 


// insertMany (queen)

db.rooms.insertOne(
    {
        name_r : "queen",
        accomdates : "4",
        price : 4000,
        description : "a room with a queen sized bed perfect for a simple getaway",
        rooms_avail : 15,
        isAvailable : false
    },
    {
        name_r : "queen",
        accomdates : "4",
        price : 4000,
        description : "a room with a queen sized bed perfect for a simple getaway",
        rooms_avail : 15,
        isAvailable : false
    },
    {
        name_r : "queen",
        accomdates : "4",
        price : 4000,
        description : "a room with a queen sized bed perfect for a simple getaway",
        rooms_avail : 15,
        isAvailable : false
    }
)



// find method for double
    db.rooms.find(
        {
            name_r : "double"
        }
    )


// updateOne method for available rooms
db.rooms.updateOne(
    {
        name_r : "queen"
    },
    {
        $set : {
            rooms_avail : 0
        }
    }
)

// delete many rooms

db.room.deleteMany(
    {
        rooms_avail : 0
    }
)